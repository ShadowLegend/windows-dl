#!/bin/bash -e

currentCondaEnvPath=$(python -c "import sys; import os; from pathlib import Path; print(Path(os.path.join(sys.prefix)).as_posix().replace('C:', '/c'))")

cmake -G "Visual Studio 16 2019" -A x64 -DENABLE_CUDA:BOOL=ON -DENABLE_CUDNN:BOOL=ON -DENABLE_CUDNN_HALF:BOOL=ON -DENABLE_OPENCV:BOOL=ON -DINSTALL_BIN_DIR=$currentCondaEnvPath/Library/bin -DINSTALL_INCLUDE_DIR=$currentCondaEnvPath/Library/include -DINSTALL_LIB_DIR=$currentCondaEnvPath/Library/lib .

cmake --build . --target install --config release