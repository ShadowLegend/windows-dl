## Deep learning setup guide for Windows

### Do the following in order

### Tools to install
+ git
+ miniconda
+ nvidia driver
+ visual studio 2019
+ cudatoolkit (see tensorflow-gpu version to match requirement)
+ cmake supports for vs2019 (install from visual studio installer)

### Library to install
+ tensorrt
+ pthreads - run `install-pthreads.sh` in git bash to download it

### Create deep learning env
+ conda env create --file requirements.yml
NOTE: From below on, use the env to build or install library in order to save those to your own env

### Build OpenCV
+ activate the env
+ run install-opencv.sh script

### Confirm if build tools and opencv works by compiling darknet
+ cloes git bash, open cmd and reactivate the env(some clib and cpath didnt get set for git bash by conda, only cmd)
+ clone darknet from [here](https://github.com/AlexeyAB/darknet)
+ open install-darknet.sh to where you cloned darknet
+ run install-darknet.sh script
+ open `cmd` and activate the env
+ test by running `darknet.exe imtest data/eagle.jpg` inside darknet source dir, since test image exists there or running `darknet.exe --version` to see if gpu and opencv being used