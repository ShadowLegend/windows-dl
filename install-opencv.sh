#!/bin/bash -e
myRepo=$(pwd)

originalPathWindows=$(python -c "import os; from pathlib import Path, PureWindowsPath; print(PureWindowsPath(Path(os.getenv('ORIGINAL_PATH'))))")
currentCondaEnvPath=$(python -c "import sys; import os; from pathlib import Path; print(Path(os.path.join(sys.prefix)).as_posix().replace('C:', '/c'))")
originalPath=$(python -c "import os; from pathlib import Path; print(Path(os.getenv('ORIGINAL_PATH')).as_posix().replace('C:', '/c').replace(';', ':'))")
condaSettedPath=$(python -c "import os; from pathlib import Path; print(Path(os.getenv('PATH')).as_posix().replace('C:', '/c').replace(';', ':'))")
currentCondaEnvPathWindows=$(python -c "import sys; import os; from pathlib import Path, PureWindowsPath; print(PureWindowsPath(Path(os.path.join(sys.prefix))))")

# pythonPath=$(which python)
# pythonIncludeDir=$(python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())")
# pythonPackagesPath=$(python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")
opencvInstallPath=$(python -c "import sys; import os; from pathlib import Path; print(Path(os.path.join(sys.prefix, 'Library', 'opencv')).as_posix())")
pythonLib=$(python -c "import sys; import os; from pathlib import Path; print(Path(os.path.join(sys.prefix, 'libs', 'python37.lib')).as_posix())")

if [  ! -d "$myRepo/opencv"  ]; then
    echo "cloning opencv"
    git clone https://github.com/opencv/opencv.git
    mkdir -p Build/opencv
else
    cd opencv
    git pull --rebase
    cd ..
fi
if [  ! -d "$myRepo/opencv_contrib"  ]; then
    echo "cloning opencv_contrib"
    git clone https://github.com/opencv/opencv_contrib.git
    mkdir -p Build/opencv_contrib
else
    cd opencv_contrib
    git pull --rebase
    cd ..
fi
RepoSource=opencv
pushd Build/$RepoSource
CMAKE_OPTIONS='-DBUILD_PERF_TESTS:BOOL=OFF\
    -DBUILD_opencv_java:BOOL=OFF\
    -DBUILD_opencv_python3:BOOL=ON\
    -DINSTALL_PYTHON_EXAMPLES:BOOL=OFF\
    -DINSTALL_C_EXAMPLES:BOOL=OFF\
    -DBUILD_PACKAGE:BOOL=ON\
    -DBUILD_TESTS:BOOL=OFF\
    -DBUILD_DOCS:BOOL=OFF\
    -DENABLE_FAST_MATH=1\
    -DCUDA_FAST_MATH=1\
    -DWITH_CUBLAS=1\
    -DBUILD_EXAMPLES:BOOL=OFF\
    -DINSTALL_CREATE_DISTRIB=ON'
cmake -G "Visual Studio 16 2019" -A x64 $CMAKE_OPTIONS -DPYTHON3_LIBRARY="$pythonLib" -DOPENCV_EXTRA_MODULES_PATH="$myRepo"/opencv_contrib/modules -DCMAKE_INSTALL_PREFIX="$opencvInstallPath" "$myRepo/$RepoSource"
# echo "************************* $Source_DIR -->debug"
# cmake --build .  --config debug
echo "************************* $Source_DIR -->release"
# cmake --build .  --config release
cmake --build .  --target install --config release
# cmake --build .  --target install --config debug

mkdir -p $currentCondaEnvPath/etc/conda/activate.d
mkdir -p $currentCondaEnvPath/etc/conda/deactivate.d

cp opencv_vars_activate.bat $currentCondaEnvPath/etc/conda/activate.d/opencv_vars.bat
cp opencv_vars_deactivate.bat $currentCondaEnvPath/etc/conda/deactivate.d/opencv_vars.bat

popd