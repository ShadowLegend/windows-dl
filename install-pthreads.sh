#!/bin/bash

currentCondaEnvPath=$(python -c "import sys; import os; from pathlib import Path; print(Path(os.path.join(sys.prefix)).as_posix().replace('C:', '/c'))")

curl -o install/pthreads.zip ftp://sourceware.org/pub/pthreads-win32/pthreads-w32-2-9-1-release.zip
unzip install/pthreads.zip -d install/pthreads
rm -f install/pthreads.zip

mv install/pthreads/Pre-built.2/lib/x64/* $currentCondaEnvPath/Library/lib/
mv install/pthreads/Pre-built.2/dll/x64/* $currentCondaEnvPath/Library/bin/
mv install/pthreads/Pre-built.2/include/* $currentCondaEnvPath/Library/include/
